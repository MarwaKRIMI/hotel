import { Component, OnInit, ViewChild } from '@angular/core';

import icMail from '@iconify/icons-ic/twotone-mail';
import icAccessTime from '@iconify/icons-ic/twotone-access-time';
import icAdd from '@iconify/icons-ic/twotone-add';
import icWhatshot from '@iconify/icons-ic/twotone-whatshot';
import icWork from '@iconify/icons-ic/twotone-work';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import icCheck from '@iconify/icons-ic/twotone-check';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import ickeyboard_arrow_up from '@iconify/icons-ic/twotone-keyboard-arrow-up';
import ickeyboard_arrow_down from '@iconify/icons-ic/twotone-keyboard-arrow-down';
import wifi from '@iconify/icons-fa-solid/wifi';
import iclocal_parking from '@iconify/icons-ic/twotone-local-parking';
import { Moment } from 'moment';
import { DaterangepickerDirective } from 'ngx-daterangepicker-material';
import icShare from '@iconify/icons-ic/twotone-share';
import icFavorite from '@iconify/icons-ic/twotone-favorite';
import calendar_alt from '@iconify/icons-fa-solid/calendar-alt';
import map_marker from '@iconify/icons-fa-solid/map-marker';
@Component({
  selector: 'vex-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class RechercheComponent implements OnInit {
  @ViewChild(DaterangepickerDirective, { static: false }) pickerDirective: DaterangepickerDirective;
  
  startDay:any;
  startMonth:any;
  startDate:any;
  endDay:any;
  endMonth:any;
  endDate:any
    icShare = icShare;
    icFavorite = icFavorite;
    displayed:boolean = false;
    paiement:boolean = false;
    frais:boolean = false;
    confirm: boolean = false;
    secure:boolean = false;
    garantie: boolean = false;
    calendar = calendar_alt;
    map_marker = map_marker;
    s:string;
    e:string;
    showChambre:boolean = false
    selected: {start: Moment, end: Moment};
    days = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
    monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];    
images:any[] = [];
images2:any[] = [];
images3:any[] = [];

icWork = icWork;
icPhone = icPhone;
parking = iclocal_parking
icPersonAdd = icPersonAdd;
icCheck = icCheck;
icMail = icMail;
icAccessTime = icAccessTime;
icAdd = icAdd;
icWhatshot = icWhatshot;
ickeyboard_arrow_up = ickeyboard_arrow_up ;
ickeyboard_arrow_down = ickeyboard_arrow_down;
wifi = wifi;
  constructor() { 

    this.images = [{path:"assets\\img\\La-Badira-Adults-Only-photos-Exterior.jpg"},{path:"assets\\img\\labadira2.jpg"}, {path:"assets\\img\\labadira3.jpg"}]
    this.images2 = [{path:"assets\\img\\Bel-Azur-photos-Exterior.jpg"},{path:"assets\\img\\belazur2.jpg"} ]
    this.images3 = [{path:"assets\\img\\Diar-Lemdina-photos-Exterior.jpg"},{path:"assets\\img\\diarlemdina2.jpg"},{path:"assets\\img\\diarlemdina3.jpg"} ,{path:"assets\\img\\diarlemdina4.jpg"}  ]
  }

  ngOnInit(): void {
    this.startDate = new Date().getDate();
    if(this.startDate < 10){
      this.s = "0"+this.startDate.toString()
    }else{
      this.s = this.startDate

    }
    this.startMonth = this.monthNames[new Date().getMonth()];
    this.startDay = this.days[new Date().getDay()];
    this.endDate = new Date().getDate()+1;
    if(this.endDate < 10){
      this.e = "0"+this.endDate.toString()
    }else{
      this.e = this.endDate

    }
    this.endMonth = this.monthNames[new Date().getMonth()];
    this.endDay = this.days[new Date().getDay()+1];




  }
  openDatepicker() {
    console.log("htrmmprpmak*gm");
    
    this.pickerDirective.open();
  }
  toggleChambre(){
    this.showChambre = !this.showChambre
  }
  onChange(event){
    console.log('start', event);
    
    console.log('start',(event?.start?._d).getMonth()+1);
    console.log('start', (event?.start?._d).getDate());
    console.log('start', (event?.start?._d).getFullYear());
    console.log('start', (event?.start?._d).getDay());

    console.log('end',(event?.end?._d).getDate())

    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
console.log(this.days[(event?.start?._d).getDay()])
console.log(this.days[(event?.end?._d).getDay()])

this.startDay = this.days[(event?.start?._d).getDay()];
this.startMonth = this.monthNames[(event?.start?._d).getMonth()];
this.startDate = (event?.start?._d).getDate();
if(this.startDate < 10){
  this.s = "0"+this.startDate.toString()
}else {
  this.s = this.startDate

}
this.endDay = this.days[(event?.end?._d).getDay()];
this.endMonth = this.monthNames[(event?.end?._d).getMonth()];
this.endDate = (event?.end?._d).getDate();
if(this.endDate < 10){
  this.e = "0"+this.endDate.toString()
}else{
  this.e = this.endDate
}
const d = new Date();
console.log("The current month is " + this.monthNames[(event?.start?._d).getMonth()]);
console.log("The upcoming month is " + this.monthNames[(event?.end?._d).getMonth()]);


    
  }
}
