import { Component, OnInit } from '@angular/core';
import icmail_outline from '@iconify/icons-ic/twotone-mail-outline';

@Component({
  selector: 'vex-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  icmail_outline = icmail_outline
  constructor() { }

  ngOnInit(): void {
  }

}
