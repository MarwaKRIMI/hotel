import { Component, OnInit } from '@angular/core';
import check from '@iconify/icons-fa-solid/check';
import iclock from '@iconify/icons-ic/twotone-lock';
import lock from '@iconify/icons-fa-solid/lock';
import ickeyboard_arrow_right from '@iconify/icons-ic/twotone-keyboard-arrow-right';

@Component({
  selector: 'vex-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {
  check = check;
  iclock = iclock;
  lock =lock;
  suite:boolean = true
  ickeyboard_arrow_right = ickeyboard_arrow_right
  constructor() { }

  ngOnInit(): void {
  }

  toggle(){
    console.log("hello");
    this.suite = !this.suite
    
  }

}
