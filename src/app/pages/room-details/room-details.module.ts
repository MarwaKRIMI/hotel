import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { IconModule } from '@visurel/iconify-angular';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { HighlightModule } from 'src/@vex/components/highlight/highlight.module';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { RoomDetailsRoutingModule } from './room-details-routing.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';

import { RoomDetailsComponent } from './room-details.component';
import { AngularSvgIconModule } from 'angular-svg-icon';


@NgModule({
  declarations: [RoomDetailsComponent],
  imports: [
    CommonModule,
    RoomDetailsRoutingModule,
    FlexLayoutModule,
    MatIconModule,
    IvyCarouselModule,
    IconModule,
    MatGridListModule,
    MatButtonModule,
    ContainerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    HighlightModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    AngularSvgIconModule.forRoot(),


  ]
})
export class RoomDetailsModule {
}
