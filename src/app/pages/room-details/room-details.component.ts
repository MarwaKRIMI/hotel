import { Component, OnInit } from '@angular/core';
import iclocal_parking from '@iconify/icons-ic/twotone-local-parking';
import icrestaurant_menu from '@iconify/icons-ic/twotone-restaurant-menu';
import volleyball_ball from '@iconify/icons-fa-solid/volleyball-ball';
import check from '@iconify/icons-fa-solid/check';
import users from '@iconify/icons-fa-solid/users';
import bath from '@iconify/icons-fa-solid/bath';
import wifi from '@iconify/icons-fa-solid/wifi';
import icMail from '@iconify/icons-ic/twotone-mail';
import icAccessTime from '@iconify/icons-ic/twotone-access-time';
import icAdd from '@iconify/icons-ic/twotone-add';
import icWhatshot from '@iconify/icons-ic/twotone-whatshot';
import icWork from '@iconify/icons-ic/twotone-work';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import icCheck from '@iconify/icons-ic/twotone-check';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import ickeyboard_arrow_up from '@iconify/icons-ic/twotone-keyboard-arrow-up';
import ickeyboard_arrow_down from '@iconify/icons-ic/twotone-keyboard-arrow-down';
import icaccess_time from '@iconify/icons-ic/twotone-access-time';
import train from '@iconify/icons-fa-solid/train';
import plane_departure from '@iconify/icons-fa-solid/plane-departure';
import bus_alt from '@iconify/icons-fa-solid/bus-alt';
import iccheck from '@iconify/icons-ic/twotone-check';
import minus from '@iconify/icons-fa-solid/minus';
import camera from '@iconify/icons-fa-solid/camera';
import baby from '@iconify/icons-fa-solid/baby';
import umbrella_beach from '@iconify/icons-fa-solid/umbrella-beach';
import swimming_pool from '@iconify/icons-fa-solid/swimming-pool';
import icchild_care from '@iconify/icons-ic/twotone-child-care';
import business_time from '@iconify/icons-fa-solid/business-time';
import { MatDialog } from '@angular/material/dialog';
import { FullCarouselComponent } from '../full-carousel/full-carousel.component';

@Component({
  selector: 'vex-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.scss']
})
export class RoomDetailsComponent implements OnInit {
  showPhoto: boolean
  showPhoto2: boolean
  showPhoto3: boolean
  showPhoto4: boolean
  showPhoto5: boolean
  showPhoto6: boolean
  parking = iclocal_parking
  restaurant = icrestaurant_menu;
  sport = volleyball_ball;
  check = check;
  users = users;
  bath = bath;
  wifi = wifi;
  business_time = business_time
  camera = camera;
  icWork = icWork;
  icPhone = icPhone;
  train = train;
  icPersonAdd = icPersonAdd;
  icCheck = icCheck;
  icMail = icMail;
  icAccessTime = icAccessTime;
  icAdd = icAdd;
  icWhatshot = icWhatshot;
  plane = plane_departure;
  ickeyboard_arrow_up = ickeyboard_arrow_up;
  ickeyboard_arrow_down = ickeyboard_arrow_down;
  icaccess_time = icaccess_time;
  minus = minus;
  baby = baby
  bus = bus_alt;
  umbrella_beach = umbrella_beach;
  swimming_pool = swimming_pool;
  icchild_care = icchild_care
  images: any[] = [];
  images2: any[] = [];
  images3: any[] = [];
  constructor(public dialog: MatDialog) {
    this.images = [{ path: "assets\\img\\La-Badira-Adults-Only-photos-Exterior.jpg" }, { path: "assets\\img\\labadira2.jpg" }, { path: "assets\\img\\labadira3.jpg" }]
    this.images2 = [{ path: "assets\\img\\Bel-Azur-photos-Exterior.jpg" }, { path: "assets\\img\\belazur2.jpg" }]
    this.images3 = [{ path: "assets\\img\\Diar-Lemdina-photos-Exterior.jpg" }, { path: "assets\\img\\diarlemdina2.jpg" }, { path: "assets\\img\\diarlemdina3.jpg" }, { path: "assets\\img\\diarlemdina4.jpg" }]
    this.showPhoto = false;
    this.showPhoto2 = false;
    this.showPhoto3 = false;
    this.showPhoto4 = false;
    this.showPhoto5 = false;
    this.showPhoto6 = false;

  }

  ngOnInit(): void {
  }
  showImages() {

    const dialogRef = this.dialog.open(FullCarouselComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      panelClass: 'stories-div',

    })

  }
}
