import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular';
import { MatButtonModule } from '@angular/material/button';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { HighlightModule } from 'src/@vex/components/highlight/highlight.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { PaiementRoutingModule } from './paiement-routing.module';
import { PaiementComponent } from './paiement.component';

@NgModule({
  declarations: [PaiementComponent],
  imports: [
    CommonModule,
    PaiementRoutingModule,
    FlexLayoutModule,
    MatIconModule,
    IvyCarouselModule,
    IconModule,
    MatGridListModule,
    MatButtonModule,
    ContainerModule,
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    HighlightModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule,
  ]
})
export class PaiementModule {
}
