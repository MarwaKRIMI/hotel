import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { MatIconModule } from '@angular/material/icon';

import { IconModule } from '@visurel/iconify-angular';
import { MatButtonModule } from '@angular/material/button';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { HighlightModule } from 'src/@vex/components/highlight/highlight.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RechercheComponent } from './recherche.component';
import { RechercheRoutingModule } from './recherche-routing.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [RechercheComponent],
  imports: [
    CommonModule,
    RechercheRoutingModule,
    FlexLayoutModule,
    MatIconModule,
    IvyCarouselModule,
    IconModule,
    MatGridListModule,
    MatButtonModule,
    ContainerModule,
    CommonModule,
    FlexLayoutModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    HighlightModule,
    IconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    NgxDaterangepickerMd.forRoot({
      format: 'DD/MM/YYYY', 
      displayFormat: 'DD/MM/YYYY', 
      direction: 'ltr', 
      weekLabel: 'S',
      separator: ' à ', 
      cancelLabel: 'Annuler', 
      applyLabel: 'OK', 
      clearLabel: 'VIDER', 
      customRangeLabel: 'Personnalisé',
      daysOfWeek: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
      monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
      firstDay: 1 
  }),   
   FormsModule,
  ]
})
export class RechercheModule {
}
