import { Component, OnInit, ViewChild } from '@angular/core';
import icShare from '@iconify/icons-ic/twotone-share';
import icFavorite from '@iconify/icons-ic/twotone-favorite';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger20ms } from 'src/@vex/animations/stagger.animation';
import calendar_alt from '@iconify/icons-fa-solid/calendar-alt';
import map_marker from '@iconify/icons-fa-solid/map-marker';
import { Moment } from 'moment';
import { DaterangepickerDirective } from 'ngx-daterangepicker-material';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'vex-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger20ms
  ],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  @ViewChild(DaterangepickerDirective, { static: false }) pickerDirective: DaterangepickerDirective;
  
startDay:any;
startMonth:any;
startDate:any;
endDay:any;
endMonth:any;
endDate:any
  icShare = icShare;
  icFavorite = icFavorite;
  displayed:boolean = false;
  paiement:boolean = false;
  frais:boolean = false;
  confirm: boolean = false;
  secure:boolean = false;
  garantie: boolean = false;
  calendar = calendar_alt;
  map_marker = map_marker;
  s:string;
  e:string;
  showChambre:boolean = false
  selected: {start: Moment, end: Moment};
  days = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];    

  constructor(private datepipe: DatePipe) { 
    console.log(this.selected);


  }

  ngOnInit(): void {
    this.startDate = new Date().getDate();
    if(this.startDate < 10){
      this.s = "0"+this.startDate.toString()
    }else{
      this.s = this.startDate

    }
    this.startMonth = this.monthNames[new Date().getMonth()];
    this.startDay = this.days[new Date().getDay()];
    this.endDate = new Date().getDate()+1;
    if(this.endDate < 10){
      this.e = "0"+this.endDate.toString()
    }else{
      this.e = this.endDate

    }
    this.endMonth = this.monthNames[new Date().getMonth()];
    this.endDay = this.days[new Date().getDay()+1];




  }
  openDatepicker() {
    this.pickerDirective.open();
  }
  toggleChambre(){
    this.showChambre = !this.showChambre
  }
  onChange(event){
    console.log('start', event);
    
    console.log('start',(event?.start?._d).getMonth()+1);
    console.log('start', (event?.start?._d).getDate());
    console.log('start', (event?.start?._d).getFullYear());
    console.log('start', (event?.start?._d).getDay());

    console.log('end',(event?.end?._d).getDate())

    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
console.log(this.days[(event?.start?._d).getDay()])
console.log(this.days[(event?.end?._d).getDay()])

this.startDay = this.days[(event?.start?._d).getDay()];
this.startMonth = this.monthNames[(event?.start?._d).getMonth()];
this.startDate = (event?.start?._d).getDate();
if(this.startDate < 10){
  this.s = "0"+this.startDate.toString()
}else {
  this.s = this.startDate

}
this.endDay = this.days[(event?.end?._d).getDay()];
this.endMonth = this.monthNames[(event?.end?._d).getMonth()];
this.endDate = (event?.end?._d).getDate();
if(this.endDate < 10){
  this.e = "0"+this.endDate.toString()
}else{
  this.e = this.endDate
}
const d = new Date();
console.log("The current month is " + this.monthNames[(event?.start?._d).getMonth()]);
console.log("The upcoming month is " + this.monthNames[(event?.end?._d).getMonth()]);


    
  }
}
