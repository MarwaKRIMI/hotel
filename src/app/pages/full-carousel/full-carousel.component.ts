import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Slide } from "./carousel/carousel.interface";
import { AnimationType } from "./carousel/carousel.animations";
import { CarouselComponent } from './carousel/carousel.component';
import icClose from '@iconify/icons-ic/twotone-close';

export interface DialogData {
  origin: string;
}
@Component({
  selector: 'vex-full-carousel',
  templateUrl: './full-carousel.component.html',
  styleUrls: ['./full-carousel.component.scss']
})
export class FullCarouselComponent implements OnInit {
  @ViewChild(CarouselComponent) carousel: CarouselComponent;
  icClose = icClose

  constructor(public dialogRef: MatDialogRef<FullCarouselComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { 


  
    }

  ngOnInit(): void {
  }


  closeDialog() {
    this.dialogRef.close();
  }
}
