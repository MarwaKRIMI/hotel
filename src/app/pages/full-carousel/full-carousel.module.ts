import { IvyCarouselModule } from 'angular-responsive-carousel';
import { CarouselComponent } from './carousel/carousel.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDividerModule } from '@angular/material/divider';
import { IconModule } from '@visurel/iconify-angular';
import { MatIconModule } from '@angular/material/icon';

import { ReactiveFormsModule } from '@angular/forms';


import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatOption } from '@angular/material/core';

import { MatSelectModule } from '@angular/material/select';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { PageLayoutModule } from 'src/@vex/components/page-layout/page-layout.module';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSliderModule } from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BreadcrumbsModule } from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import { SecondaryToolbarModule } from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import { MatDialogModule } from '@angular/material/dialog';
import { FullCarouselComponent } from './full-carousel.component';
import { FullCarouselRoutingModule } from './ful-carousel-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [FullCarouselComponent, CarouselComponent],
  imports: [
    CommonModule,
    FullCarouselRoutingModule,
    ContainerModule,
    FlexLayoutModule,
    MatDividerModule,
    MatIconModule,
    IconModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatSelectModule,

    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    PageLayoutModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatIconModule,
    IconModule,
    MatInputModule,


    FlexLayoutModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSliderModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSlideToggleModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    ContainerModule,
    MatDialogModule,
    IvyCarouselModule,
    MDBBootstrapModule.forRoot()





  ]
})
export class FullCarouselModule {
}
