import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullCarouselComponent } from './full-carousel.component';
const routes: Routes = [
  {
    path: '',
    component: FullCarouselComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FullCarouselRoutingModule {
}
