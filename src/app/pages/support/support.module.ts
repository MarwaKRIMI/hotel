import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { IconModule } from '@visurel/iconify-angular';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { HighlightModule } from 'src/@vex/components/highlight/highlight.module';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SupportComponent } from './support.component';
import { SupportRoutingModule } from './support-routing.module';
import { MatSelectModule } from '@angular/material/select';



@NgModule({
  declarations: [SupportComponent],
  imports: [
    CommonModule,
    SupportRoutingModule,
    FlexLayoutModule,
    MatIconModule,
    IvyCarouselModule,
    IconModule,
    MatGridListModule,
    MatButtonModule,
    ContainerModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    HighlightModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule

  ]
})
export class SupportModule {
}
